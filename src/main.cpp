#include <main.h>

using namespace std;

#if defined(_WIN32) || defined(WIN32)
	
	#include <conio.h>
	
	void init()
	{
		
	}
	
	void exit()
	{
		
	}
	
#else
	
	#include <sys/ioctl.h>
	
	#include <termios.h>
	#include <unistd.h>
	
	static struct termios revert;
	
	void init()
	{
		static struct termios newt;
		
		tcgetattr(0, &revert);
		newt = revert;
		
		newt.c_lflag &= ~ICANON;
		newt.c_lflag &= ~ECHO;
		tcsetattr(0, TCSANOW, &newt);
		setbuf(stdin, NULL);
	}
	
	void exit()
	{
		tcsetattr(0, TCSADRAIN, &revert);
	}
	
	char getch()
	{
		return cin.get();
	}
	
	int kbhit()
	{
		int bytesWaiting;
		ioctl(0, FIONREAD, &bytesWaiting);
		return bytesWaiting;
	}
	
#endif

CPU cpu;
Memory mem;

u16 romPC;

bool inOS;
bool quit;

char c;

int main(int argc, char** argv)
{
	init();
	
	cpu = CPU();
	mem = Memory();
	
	quit = false;
	
	if (argc > 1)
	{
		mem.loadROM(argv[1]);
	}
	
	else
	{
		printf("usage: lc3 [rom path]\n");
		
		exit();
		
		return 1;
	}
	
	romPC = cpu.pc;
	
	if (mem.loadROM("./lc3os.bin"))
	{
		inOS = true;
		
		cpu.pc = 0x200;
	}
	
	else
	{
		cout << endl;
		
		exit();
		
		return 1;
	}
	
	while (!quit)
	{
		if (kbhit())
		{
			c = getch();
			
			mem.mem[Memory::KBSR] = 0x8000;
			mem.mem[Memory::KBDR] = (u16) c;
		}
		
		cpu.cycle();
		
		if (mem.mem[Memory::DSR] >> 15 && mem.mem[Memory::DDR] != 0x0000)
		{
			putchar((char) mem.mem[Memory::DDR]);
			cout << flush;
			
			mem.mem[Memory::DDR] = 0x0000;
		}
	}
	
	exit();
	
	cout << endl;
}